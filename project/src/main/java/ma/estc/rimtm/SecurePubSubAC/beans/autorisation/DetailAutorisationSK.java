package ma.estc.rimtm.SecurePubSubAC.beans.autorisation;

import java.io.Serializable;
import java.security.cert.X509Certificate;

import ma.estc.rimtm.SecurePubSubAC.beans.DetailMessage;

public class DetailAutorisationSK implements DetailMessage,Serializable{

	private X509Certificate clientCertificate;
	private String ResponceTopic;
	public X509Certificate getClientCertificate() {
		return clientCertificate;
	}
	public void setClientCertificate(X509Certificate clientCertificate) {
		this.clientCertificate = clientCertificate;
	}
	public String getResponceTopic() {
		return ResponceTopic;
	}
	public void setResponceTopic(String responceTopic) {
		ResponceTopic = responceTopic;
	}
	
	
	
}
