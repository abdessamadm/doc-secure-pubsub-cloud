package ma.estc.rimtm.SecurePubSubAC.beans;

import java.io.Serializable;

public class MessageSimple implements DetailMessage, Serializable {

	private String clinetName;
	private String Text;

	public String getClinetName() {
		return clinetName;
	}

	public void setClinetName(String clinetName) {
		this.clinetName = clinetName;
	}

	public String getText() {
		return Text;
	}

	public void setText(String text) {
		Text = text;
	}

}
