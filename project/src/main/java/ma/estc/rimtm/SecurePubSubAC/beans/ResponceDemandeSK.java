package ma.estc.rimtm.SecurePubSubAC.beans;

import java.io.Serializable;
import java.security.cert.X509Certificate;

public class ResponceDemandeSK implements DetailMessage, Serializable {
	private EncreptedObject encreptedMessage;
	private String topic;

	public EncreptedObject getEncreptedMessage() {
		return encreptedMessage;
	}

	public void setEncreptedMessage(EncreptedObject encreptedMessage) {
		this.encreptedMessage = encreptedMessage;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	

}
