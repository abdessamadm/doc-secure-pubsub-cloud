package ma.estc.rimtm.SecurePubSubAC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.estc.rimtm.SecurePubSubAC.core.OnMessageArrivedControle;
import ma.estc.rimtm.SecurePubSubAC.core.RequesTraitement;
import ma.estc.rimtm.SecurePubSubAC.core.RequestTraitmentEventPublishHADOOPImpl;
import ma.estc.rimtm.SecurePubSubAC.core.RequestTraitmentEventPublishImpl;
import ma.estc.rimtm.SecurePubSubAC.core.util.Constantes;
import ma.estc.rimtm.SecurePubSubAC.core.util.Utils;
import ma.estc.rimtm.SecurePubSubAC.exception.SecurePubSubException;
import ma.estc.rimtm.SecurePubSubAC.pubsub.mqtt.MqttPublishSubcripeImpl;

public class Main {
	
	//java -jar SecurePubSubAC-Main-jar-with-dependencies.jar  "USER1" "aaaa" "/Users/absessamadmektoubi/git/doc-secure-pubsub-cloud/project/src/main/resources/cle/KeyStore/g1/user1/KeyStore"   "" "tcp://127.0.0.1:1883" "false"
	
	public static void main(String[] args) throws SecurePubSubException {
		Constantes constantes=new Constantes();
    	Map<String , String> map=new HashMap<String, String>();
    	
//    	map.put("CLIENT_ID", "EC");
//    	map.put("STORE_KEY", "aaaa");
//    	map.put("STORE_PATH", "/Users/absessamadmektoubi/git/doc-secure-pubsub-cloud/project/src/main/resources/cle/KeyStore/ec/KeyStore");
//    	map.put("XACML_PATH", "/Users/absessamadmektoubi/git/doc-secure-pubsub-cloud/project/src/main/resources/testsets/conformance/xacml3.0-ct-v.0.4");
//    	map.put("BROKER", "tcp://127.0.0.1:1883");
//    	map.put("AT", "true");
    	
    	map.put("CLIENT_ID", args[0]);
    	map.put("STORE_KEY", args[1]);
    	map.put("STORE_PATH",args[2] );
    	map.put("XACML_PATH", args[3]);
    	map.put("BROKER",  args[4]);
    	map.put("AT",  args[5]);
    	
    	constantes.setMap(map);
    	
    	List<String> lListopic=new ArrayList<String>();
    	
    	
    	RequesTraitement requesTraitement=new RequesTraitement(
    			new RequestTraitmentEventPublishHADOOPImpl(map.get("BROKER"),map.get("CLIENT_ID")),
    			constantes,
    			lListopic,map.get("XACML_PATH"));
    	
    	MqttPublishSubcripeImpl clientMqtt=new MqttPublishSubcripeImpl(map.get("BROKER"), Utils.getRondom20());
    	clientMqtt.setOnMessageArrived(new OnMessageArrivedControle(requesTraitement));
    	clientMqtt.init();
    	clientMqtt.subcripe(map.get("CLIENT_ID")+"_controle");
	}

}
