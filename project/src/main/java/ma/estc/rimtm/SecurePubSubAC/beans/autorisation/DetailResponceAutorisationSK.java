package ma.estc.rimtm.SecurePubSubAC.beans.autorisation;

import java.io.Serializable;
import java.security.cert.X509Certificate;

import ma.estc.rimtm.SecurePubSubAC.beans.DetailMessage;
import ma.estc.rimtm.SecurePubSubAC.beans.EncreptedObject;

public class DetailResponceAutorisationSK implements DetailMessage,Serializable{

	private X509Certificate clientCertificate;
	private String topic;
	private EncreptedObject responce;
	public X509Certificate getClientCertificate() {
		return clientCertificate;
	}
	public void setClientCertificate(X509Certificate clientCertificate) {
		this.clientCertificate = clientCertificate;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public EncreptedObject getResponce() {
		return responce;
	}
	public void setResponce(EncreptedObject responce) {
		this.responce = responce;
	}
	
	
	
	
	
}
