package ma.estc.rimtm.SecurePubSubAC.core;

import ma.estc.rimtm.SecurePubSubAC.beans.Message;
import ma.estc.rimtm.SecurePubSubAC.core.util.crypto.UtilCrypto;

public class OnMessageArrivedImpl implements OnMessageArrived {

	private RequesTraitement mRequesTraitement;
	private UtilCrypto utilCrypto;

	public OnMessageArrivedImpl(RequesTraitement pRequesTraitement, String keyStore) {
		this.mRequesTraitement = pRequesTraitement;
		utilCrypto = new UtilCrypto(keyStore);
	}

	public void OnMessageArrived(String pTopic, byte[] pPlayLoad) {
		// remove duplicate from implimentation
		Message message = (Message) utilCrypto.getObjectFromByte(pPlayLoad);
		int lMessageType = message.getTypeMessage();
		if (!message.getClient().equals(mRequesTraitement.getClientId())
				&& !mRequesTraitement.isAutoriteDeConfiance()) {
			//System.out.println(lMessageType);
			if (lMessageType == Message.SIMPLE_MESSAGE) {
				mRequesTraitement.readSimpleMessage(message, pTopic);
			}
			if (lMessageType == Message.ENCREPTED_MESSAGE) {
				mRequesTraitement.readEncreptedMessage(message, pTopic);
			}
			if (lMessageType == Message.DEMANDE_PUBLIC_KEY) {
				mRequesTraitement.readDemandePk(message);
			}
			if (lMessageType == Message.RESPONCE_PUBLIC_KEY) {
				mRequesTraitement.readResponcePK(message);
			}
			if (lMessageType == Message.DEMANDE_PRIVATE_KEY) {
				mRequesTraitement.readDemandeSK(message);
			}
			if (lMessageType == Message.RESPONCE_PRIVATE_KEY) {
				mRequesTraitement.readResponcSK(message);
			}
			if (lMessageType == Message.RESPONCE_AUTORISATION_PRIVATE_KEY) {
				mRequesTraitement.readResponceDemandeAutorisation(message);
			}

		} else if (!message.getClient().equals(mRequesTraitement.getClientId())
				&& mRequesTraitement.isAutoriteDeConfiance()) {
			{
				if (lMessageType == Message.DEMANDE_AUTORISATION_PRIVATE_KEY) {
					mRequesTraitement.readDemandeAutorisation(message);
				}
				
			}
		}
	}

}
