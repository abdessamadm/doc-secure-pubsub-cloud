package ma.estc.rimtm.SecurePubSubAC.core;

import java.util.Iterator;
import java.util.Map;

import com.att.research.xacml.api.Decision;

import ma.estc.rimtm.SecurePubSubAC.beans.DemandePK;
import ma.estc.rimtm.SecurePubSubAC.beans.DemandeSK;
import ma.estc.rimtm.SecurePubSubAC.beans.Message;
import ma.estc.rimtm.SecurePubSubAC.beans.MessageSimple;
import ma.estc.rimtm.SecurePubSubAC.beans.ResponceDemandePK;
import ma.estc.rimtm.SecurePubSubAC.beans.ResponceDemandeSK;
import ma.estc.rimtm.SecurePubSubAC.core.util.Utils;
import ma.estc.rimtm.SecurePubSubAC.pubsub.mqtt.MqttPublishSubcripeImpl;

public class RequestTraitmentEventPublishImpl implements RequestTraitmentEvent {
	
	private MqttPublishSubcripeImpl clientMqtt ;
	private String clientName ;
	private String logTopic ;
	
	public RequestTraitmentEventPublishImpl(String pBroker,String pClientName) {
		clientName=pClientName;
		clientMqtt=new MqttPublishSubcripeImpl(pBroker, Utils.getRondom20());
		clientMqtt.init();
		logTopic=pClientName+"_log";
	}
	

	public void onSimpleMessageArrived(MessageSimple pMessageSimple, String pTopic) {
		String message=clientName+" SimpleMessageArrived "+" Topic '"+pTopic+"' Message : '"+pMessageSimple.getText()+"'";
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
	}

	public void onEncreptedMessageArrived(MessageSimple pMessageSimple, String pTopic, boolean isDecrepted) {
		String message=clientName+" EncreptedMessageArrived "+" Topic '"+pTopic;
		if(isDecrepted) {
			 message+=clientName+" EncreptedMessageArrived "+" Topic '"+pTopic+"' Message : '"+pMessageSimple.getText()+"'";
		}else {
			 message+="' key of topic not found '";
		}
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
	}

	public void onDemandePublickeyArrived(DemandePK lDemandePK, Message lMessageResponce, String pTopic,
			boolean isTraitement) {
		String message=clientName+" DemandePublickeyArrived "+" Topic '"+pTopic+"' Topic Respense : '"+lDemandePK.getResponceTopic()+"'";
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
	}

	public void onResponcePublickeyArrived(ResponceDemandePK pResponceDemandePK, String pTopic, boolean isTraitement) {
		String message=clientName+" onResponcePublickeyArrived "+" Topic : "+pTopic+" '";
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
	}

	public void onDemandePrivateKeyArrived(DemandeSK lDemandeSK, ResponceDemandeSK lResponceDemandeSK,
			boolean isTraitement) {
		
		String message=clientName+" onDemandePrivateKey "+" Topic : "+lDemandeSK.getTopic()+" '";
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
		
	}

	public void onResponcePrivateKeyArrived(ResponceDemandeSK lResponDemandeSK, boolean b) {
		String message="";
		if(b) {
			 message=clientName+" onResponcePrivateKey "+" Topic : "+lResponDemandeSK.getTopic()+" '";
		}else {
			message=clientName+" onResponcePrivateKey "+" Topic : "+lResponDemandeSK.getTopic()+"  une erreur est servenue'";
		}
		
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
		
	}


	@Override
	public void onRun(Map<String, String> pParams) {
		String message=clientName+" onRun '";
		Iterator it = pParams.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        message +=" "+ pair.getKey() + " = " + pair.getValue()+" ";
	    }
	    message+="' ";
	    
	    clientMqtt.publish(message.getBytes(), logTopic);
	    
		
	}


	@Override
	public void onSendDemandePublicKey(String pTopic) {
		String message=clientName+" onSendDemandePublicKey "+" Topic : "+pTopic+" '";
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
		
	}


	@Override
	public void OnsendDemandeAutorisation(String pTopic) {
		String message=clientName+" OnsendDemandeAutorisation "+" Topic : "+pTopic+" '";
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
	}


	@Override
	public void readDemandeAutorisation(String user, String topic, String operation, Decision reponce) {
		String message=clientName+" readDemandeAutorisation user "+user+" Topic : "+topic+"  operation "+operation+ " Decision : "+reponce.toString();
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
	}


	@Override
	public void OnReadResponceDemandeAutorisation(String pTopic) {
		String message=clientName+" OnReadResponceDemandeAutorisation "+" Topic : "+pTopic+" '";
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
	}


	@Override
	public void onSendDemandeSecreteKey(String pTopic) {
		String message=clientName+" onSendDemandeSecreteKey "+" Topic : "+pTopic+" '";
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
		
	}
	
	@Override
	public void onSendEncreptedMessage(String pTopic,String mesage) {
		String message=clientName+" OnSendEncreptedMessage "+" Topic : "+pTopic+" Message "+mesage+" '";
		System.out.println(message);
		clientMqtt.publish(message.getBytes(), logTopic);
	}
	
}
