package ma.estc.rimtm.SecurePubSubAC.pubsub;

import ma.estc.rimtm.SecurePubSubAC.core.OnMessageArrived;

public interface PublishSubcripe {
	
	public boolean publish(byte[] pMessage,String topic);
	public void subcripe(String topic[]);
	public void unsubcripe(String topic);
	public boolean  subcripe(String topic);
	public Boolean init();
	public void setOnMessageArrived(OnMessageArrived pOnMessageArrived);
	
}
