package ma.estc.rimtm.SecurePubSubAC.beans.autorisation;

import java.io.Serializable;
import java.security.cert.X509Certificate;

import ma.estc.rimtm.SecurePubSubAC.beans.DetailMessage;
import ma.estc.rimtm.SecurePubSubAC.beans.EncreptedObject;

public class DetailResponceAutorisationCryptedSK implements DetailMessage, Serializable {

	private EncreptedObject detailResponceAutorisationCryptedSK;

	public EncreptedObject getDetailResponceAutorisationCryptedSK() {
		return detailResponceAutorisationCryptedSK;
	}

	public void setDetailResponceAutorisationCryptedSK(EncreptedObject detailResponceAutorisationCryptedSK) {
		this.detailResponceAutorisationCryptedSK = detailResponceAutorisationCryptedSK;
	}
	
}
