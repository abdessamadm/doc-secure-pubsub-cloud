package ma.estc.rimtm.SecurePubSubAC.core.util;

import java.util.HashMap;
import java.util.Map;

public class Constantes {

	private Map<String,String> map=new HashMap<String, String>();
	
	public String getString(String pKey) {
		return map.get(pKey);
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}
	
  
}