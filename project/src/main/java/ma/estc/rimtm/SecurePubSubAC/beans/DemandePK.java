package ma.estc.rimtm.SecurePubSubAC.beans;

import java.io.Serializable;

public class DemandePK implements DetailMessage, Serializable {

	private String topic;
	private String responceTopic;

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getResponceTopic() {
		return responceTopic;
	}

	public void setResponceTopic(String responceTopic) {
		this.responceTopic = responceTopic;
	}

}
