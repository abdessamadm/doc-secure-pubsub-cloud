package ma.estc.rimtm.SecurePubSubAC.core.util;

import java.security.SecureRandom;
import java.util.List;

public class Utils {
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();

	public static String getRondom20() {
	   return getRondom(20);
	}
	
	public static String getRondom(int len) {
		StringBuilder sb = new StringBuilder( len );
		   for( int i = 0; i < len; i++ ) 
		      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		   return sb.toString();
	}
	
	public static   String[] listToArray(List<String> list) {
		String[] itemsArray =(String[]) new String[list.size()];
        itemsArray = list.toArray(itemsArray);
        return itemsArray;
	}
	
}
