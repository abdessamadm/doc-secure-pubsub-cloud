package ma.estc.rimtm.SecurePubSubAC.beans;

import java.io.Serializable;
import java.security.cert.X509Certificate;

import ma.estc.rimtm.SecurePubSubAC.beans.autorisation.DemandeAutorisationSK;
import ma.estc.rimtm.SecurePubSubAC.beans.autorisation.DetailResponceAutorisationSignierSK;

public class DetailDemandeSK implements DetailMessage, Serializable {

	private DetailResponceAutorisationSignierSK detailResponceAutorisationSignierSK;
	private String ResponceTopic;

	public DetailResponceAutorisationSignierSK getDetailResponceAutorisationSignierSK() {
		return detailResponceAutorisationSignierSK;
	}

	public void setDetailResponceAutorisationSignierSK(
			DetailResponceAutorisationSignierSK detailResponceAutorisationSignierSK) {
		this.detailResponceAutorisationSignierSK = detailResponceAutorisationSignierSK;
	}

	public String getResponceTopic() {
		return ResponceTopic;
	}

	public void setResponceTopic(String responceTopic) {
		ResponceTopic = responceTopic;
	}

}
