package ma.estc.rimtm.SecurePubSubAC.beans.autorisation;

import java.io.Serializable;

import ma.estc.rimtm.SecurePubSubAC.beans.DetailMessage;

/**
 * @author Abdessamad
 *
 */
public class DetailResponceAutorisationSignierSK implements DetailMessage, Serializable {

	private DetailResponceAutorisationSK detailResponceAutorisationSK;
	private byte[]  signature;

	public DetailResponceAutorisationSK getDetailResponceAutorisationSK() {
		return detailResponceAutorisationSK;
	}

	public void setDetailResponceAutorisationSK(DetailResponceAutorisationSK detailResponceAutorisationSK) {
		this.detailResponceAutorisationSK = detailResponceAutorisationSK;
	}

	public byte[] getSignature() {
		return signature;
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}
	
	

}
