package ma.estc.rimtm.SecurePubSubAC.xacml;

import java.io.File;
import java.util.Iterator;

import com.att.research.xacml.api.Attribute;
import com.att.research.xacml.api.AttributeValue;
import com.att.research.xacml.api.Decision;
import com.att.research.xacml.api.Request;
import com.att.research.xacml.api.RequestAttributes;
import com.att.research.xacml.api.Response;
import com.att.research.xacml.api.Result;
import com.att.research.xacml.api.pdp.PDPEngine;
import com.att.research.xacml.api.pdp.PDPEngineFactory;
import com.att.research.xacml.std.StdAttributeValue;
import com.att.research.xacml.std.StdMutableAttribute;
import com.att.research.xacml.std.StdMutableRequest;
import com.att.research.xacml.std.StdMutableRequestAttributes;
import com.att.research.xacml.std.StdRequest;
import com.att.research.xacml.std.dom.DOMProperties;
import com.att.research.xacml.util.XACMLProperties;
import com.att.research.xacmlatt.pdp.std.StdPolicyFinderFactory;

public class XacmlPEP {
	PDPEngineFactory pdpEngineFactory;
	
	public Decision run(String user,String topic,String action,String pathDir) throws Exception {
		 
		Decision decision=null;
		PDPEngineFactory thisPDPEngineFactory	= null;
		thisPDPEngineFactory	= this.getPDPEngineFactory();
		
		DOMProperties.setLenient(true);
		StdMutableRequest stdMutableRequest	= DOMRequestAC.load(new File(pathDir+"/IIA001Request.xml"));
		
		StdMutableRequest stdMutableRequestIn	=new StdMutableRequest();
		
	 
		Iterator<RequestAttributes> iterator=  stdMutableRequest.getRequestAttributes().iterator();
		while(iterator.hasNext()) {
			RequestAttributes requestAttributes=iterator.next();
			StdMutableRequestAttributes stdMutableRequestAttributes=new StdMutableRequestAttributes();
			stdMutableRequestAttributes.setCategory(requestAttributes.getCategory());
			stdMutableRequestAttributes.setContentRoot(requestAttributes.getContentRoot());
			stdMutableRequestAttributes.setXmlId(requestAttributes.getXmlId());
			
			Iterator<Attribute> iteratorAtt= requestAttributes.getAttributes().iterator();
			while(iteratorAtt.hasNext()) {
				Attribute attribute=iteratorAtt.next();
				StdMutableAttribute stdMutableAttribute=new StdMutableAttribute();
				stdMutableAttribute.setAttributeId(attribute.getAttributeId());
				stdMutableAttribute.setCategory(attribute.getCategory());
				stdMutableAttribute.setIncludeInResults(attribute.getIncludeInResults());
				stdMutableAttribute.setIssuer(attribute.getIssuer());
				Iterator<AttributeValue<?>> iteratorAttVal= attribute.getValues().iterator();
				
				
				while(iteratorAttVal.hasNext()) {
					AttributeValue<?> attributeValue=iteratorAttVal.next();
					System.out.println(attribute.getCategory());
					 
					if(attribute.getCategory().toString().equals("urn:oasis:names:tc:xacml:1.0:subject-category:access-subject")) {
						StdAttributeValue<?> staattributeValue=new StdAttributeValue(attributeValue.getDataTypeId(), user, attributeValue.getXPathCategory());
						stdMutableAttribute.addValue(staattributeValue);
					}
					
					if(attribute.getCategory().toString().equals("urn:oasis:names:tc:xacml:3.0:attribute-category:resource")) {
						StdAttributeValue<?> staattributeValue=new StdAttributeValue(attributeValue.getDataTypeId(), topic, attributeValue.getXPathCategory());
						stdMutableAttribute.addValue(staattributeValue);
					}
					
					if(attribute.getCategory().toString().equals("urn:oasis:names:tc:xacml:3.0:attribute-category:action")) {
						StdAttributeValue<?> staattributeValue=new StdAttributeValue(attributeValue.getDataTypeId(), action, attributeValue.getXPathCategory());
						stdMutableAttribute.addValue(staattributeValue);
					}
					
					
				}
				stdMutableRequestAttributes.add(stdMutableAttribute);
			}
			stdMutableRequestIn.add(stdMutableRequestAttributes);
		}
		
		Iterator<RequestAttributes> i=  stdMutableRequest.getRequestAttributes().iterator();
		while(i.hasNext()) {
			 
			Iterator<Attribute> ii= i.next().getAttributes().iterator();
			while(ii.hasNext()) {
				Attribute attribute=ii.next();
				Iterator<AttributeValue<?>> iii= attribute.getValues().iterator();
				while(iii.hasNext()) {
					AttributeValue<?> attributeValue=iii.next();
					System.out.println(attributeValue.getValue());
				}
			}
		}
		
		Request request			= new StdRequest(stdMutableRequest);
		
		
		setXACMLProperties(pathDir+"/IIA001Policy.xml");
		
		//XACMLProperties.setProperty("xacml.pip.engines","OntologiePIPEngine");
		//XACMLProperties.setProperty("OntologiePIPEngine.classname","ma.estc.rimtm.SecurePubSubAC.xacml.OntologiePIPEngine");
		
		PDPEngine pdpEngine		= thisPDPEngineFactory.newEngine();
		 
		Response	response	= pdpEngine.decide(request);
		Iterator<Result> s=response.getResults().iterator();
		while (s.hasNext()) {
			Result type = (Result) s.next();
			System.out.println(type.getDecision());
			decision=type.getDecision();
		}
		
		return decision;
	}
  protected PDPEngineFactory getPDPEngineFactory() throws Exception {
		
		if (this.pdpEngineFactory == null) {
			this.pdpEngineFactory	= PDPEngineFactory.newInstance();
		}
		return this.pdpEngineFactory;
	}
	

	private void setXACMLProperty(String propertyName, File listFiles) {
	 
		StringBuilder stringBuilderIdList	= new StringBuilder();
		 
			File file	=listFiles;
			if (stringBuilderIdList.length() > 0) {
				stringBuilderIdList.append(',');
			}
			stringBuilderIdList.append(file.getName());
			
			XACMLProperties.setProperty(file.getName() + StdPolicyFinderFactory.PROP_FILE, file.getAbsolutePath());
			XACMLProperties.setProperty(propertyName, stringBuilderIdList.toString());
	}
	
	 
	public void setXACMLProperties(String rootPolicies) {
		setXACMLProperty(XACMLProperties.PROP_ROOTPOLICIES, new File(rootPolicies));
		 
	}
}
