package ma.estc.rimtm.SecurePubSubAC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.estc.rimtm.SecurePubSubAC.core.OnMessageArrivedControle;
import ma.estc.rimtm.SecurePubSubAC.core.RequesTraitement;
import ma.estc.rimtm.SecurePubSubAC.core.RequestTraitmentEventPublishHADOOPImpl;
import ma.estc.rimtm.SecurePubSubAC.core.util.Constantes;
import ma.estc.rimtm.SecurePubSubAC.core.util.Utils;
import ma.estc.rimtm.SecurePubSubAC.exception.SecurePubSubException;
import ma.estc.rimtm.SecurePubSubAC.pubsub.mqtt.MqttPublishSubcripeImpl;

public class User2G2 
{
    public static void main( String[] args ) throws SecurePubSubException
    {
    	
    	 
    	Constantes constantes=new Constantes();
    	Map<String , String> map=new HashMap<String, String>();
    	
    	map.put("CLIENT_ID", "USER2");
    	map.put("STORE_KEY", "aaaa");
    	map.put("STORE_PATH", "/Users/absessamadmektoubi/git/doc-secure-pubsub-cloud/project/src/main/resources/cle/KeyStore/g1/user2/KeyStore");
    	map.put("BROKER", "tcp://127.0.0.1:1883");
    	map.put("AT", "false");
    	
    	constantes.setMap(map);
    	
    	List<String> lListopic=new ArrayList<String>();
    	//lListopic.add("topic1");
    	
    	RequesTraitement requesTraitement=new RequesTraitement(new RequestTraitmentEventPublishHADOOPImpl(map.get("BROKER"),map.get("CLIENT_ID")),constantes,lListopic,null);
    	
    	//requesTraitement.sendSimpleMessage("sss", "topic22");
    	MqttPublishSubcripeImpl clientMqtt=new MqttPublishSubcripeImpl(map.get("BROKER"), Utils.getRondom20());
    	clientMqtt.setOnMessageArrived(new OnMessageArrivedControle(requesTraitement));
    	clientMqtt.init();
    	clientMqtt.subcripe(map.get("CLIENT_ID")+"_controle");
    	 
    }
    
    
}
