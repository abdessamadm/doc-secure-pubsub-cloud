package ma.estc.rimtm.SecurePubSubAC.core.util.crypto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import ma.estc.rimtm.SecurePubSubAC.beans.EncreptedObject;

public class UtilCrypto {
	
	
	private String IV = "AAAAAAAAAAAAAAAA";
	private String KEY_STORE_PATH ;
	
	
	
	
	public UtilCrypto(String kEY_STORE_PATH) {
		super();
		KEY_STORE_PATH = kEY_STORE_PATH;
	}

	public  String getKeyStorePath() {
		return KEY_STORE_PATH;
	}
	
	public  byte[] getByteOfObject(Object object) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		byte[] yourBytes = null;
		try {

			out = new ObjectOutputStream(bos);
			out.writeObject(object);
			yourBytes = bos.toByteArray();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			try {
				bos.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return yourBytes;
	}

	public  Object getObjectFromByte(byte[] yourBytes) {
		ByteArrayInputStream bis = new ByteArrayInputStream(yourBytes);
		ObjectInput in = null;
		Object o = null;
		try {
			in = new ObjectInputStream(bis);
			o = in.readObject();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return o;
	}
	
	public  KeyStore loadKeyStore(String pass) throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		FileInputStream is = new FileInputStream(getKeyStorePath());
		KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
		keystore.load(is, pass.toCharArray());
		is.close();
		return keystore;
	}
	
	//getPrivatekey2
	public  boolean isPrivateKeyInKeyStore(String pPass, String pAlice) {
		KeyStore ks;
		boolean retu = false;
		try {
			retu = getPrivateKeyWithAlice(  pPass,   pAlice) == null ? false : true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return retu;

	}
	
	////getPrivatekey1
	public  PrivateKey getPrivateKeyWithAlice(String pPass, String pAlice) throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, InvalidKeySpecException, UnrecoverableKeyException {
		KeyStore ks = loadKeyStore(pPass);
		return (PrivateKey) ks.getKey(pAlice, pPass.toCharArray());
	}
	
	public  Certificate getX509FromKeyStore(String pPasse, String pAlias) {
		try {
			KeyStore keystore = loadKeyStore(pPasse);
			Certificate certs = keystore.getCertificate(pAlias);
			return certs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public  boolean isX509FromKeyStore(String pPasse, String pAlias) {
			return getX509FromKeyStore(pPasse, pAlias)==null?false:true;
	}
	
	public  Object getDecreptedObject(EncreptedObject pEncreptedObject, PrivateKey pPrivateKey) {
		String secretKey = (String) getObjectFromByte(decrypt(pEncreptedObject.getKey(), pPrivateKey));
		Object bs = getObjectFromByte(decryptSymetrique(pEncreptedObject.getObject(), secretKey));
		return bs;
	}
	
	public  byte[] decrypt(byte[] text, PrivateKey key) {
		byte[] dectyptedText = null;
		try {
			final Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, key);
			dectyptedText = cipher.doFinal(text);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return dectyptedText;
	}
	
	public  byte[] decryptSymetrique(byte[] cipherText, String encryptionKey)   {
		byte[] b = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
			return cipher.doFinal(cipherText);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return b;
	}
	
	public  boolean addX509ToKeyStore(String pPasse, Certificate pCertfile, String pAlias) {
		try {
			KeyStore keystore = loadKeyStore(pPasse);
			keystore.setCertificateEntry(pAlias, pCertfile);
			updateKeyStore(pPasse, keystore);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}
	
	public  void updateKeyStore(String pPass, KeyStore keyStore) throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		char[] password = pPass.toCharArray();
		FileOutputStream out = new FileOutputStream(getKeyStorePath());
		keyStore.store(out, password);
		out.close();
	}
	
	public  EncreptedObject getEncreptedObject(Object object, PublicKey key) {
		String skey = getSecureRandomString();
		byte[] bs = encryptSymetrique(getByteOfObject(object), skey);
		byte[] sk = encrypt(getByteOfObject(skey), key);

		EncreptedObject encreptedMessage = new EncreptedObject();
		encreptedMessage.setKey(sk);
		encreptedMessage.setObject(bs);
		return encreptedMessage;
	}
	
	public  String getSecureRandomString()   {
		 SecureRandom random = new SecureRandom();
		 String s= new BigInteger(130, random).toString(32);
		 return s.substring(0, 16);
	}
	
	public  byte[] encryptSymetrique(byte[] plainText, String encryptionKey)   {
		byte[] b = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
			cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
			return cipher.doFinal(plainText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}
	
	public  byte[] encrypt(byte[] text, PublicKey key) {
		byte[] cipherText = null;
		try {
			final Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText = cipher.doFinal(text);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cipherText;
	}
	
	public  void addPrivateKet(  String pass, String aliasCertifica, String aliaceKey, PrivateKey key) throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, InvalidKeySpecException ,Exception{
		KeyStore ks = loadKeyStore(pass);
		Certificate[] certArray = new Certificate[1];
		certArray[0] = getX509FromKeyStore(pass, aliasCertifica);
		boolean isValid=verifyKey(key,(X509Certificate)certArray[0]);
		if(isValid){
			ks.setKeyEntry(aliaceKey, key, pass.toCharArray(), certArray);
			updateKeyStore(pass, ks);
		}else {
			throw new Exception("Rong key");
		}
	}
	
	private  boolean verifyKey(PrivateKey key,X509Certificate aliasCertifica) {
		String s="verifyKey";
		try {
			return new String(decrypt(encrypt(s.getBytes(), aliasCertifica.getPublicKey()),key)).equals(s);
		} catch (Exception e) {
			return false;
		}
	}
	
	public byte[] sign(byte[] data, PrivateKey privateKey) throws InvalidKeyException, Exception{
		Signature rsa = Signature.getInstance("SHA1withRSA"); 
		rsa.initSign(privateKey);
		rsa.update(data);
		return rsa.sign();
	}
}
