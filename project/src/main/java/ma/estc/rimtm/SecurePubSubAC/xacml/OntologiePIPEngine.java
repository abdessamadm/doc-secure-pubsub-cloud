package ma.estc.rimtm.SecurePubSubAC.xacml;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.att.research.xacml.api.pip.PIPException;
import com.att.research.xacml.api.pip.PIPFinder;
import com.att.research.xacml.api.pip.PIPRequest;
import com.att.research.xacml.api.pip.PIPResponse;
import com.att.research.xacml.std.StdAttributeValue;
import com.att.research.xacml.std.StdMutableAttribute;
import com.att.research.xacml.std.pip.StdMutablePIPResponse;
import com.att.research.xacml.std.pip.StdSinglePIPResponse;
import com.att.research.xacml.std.pip.engines.StdConfigurableEngine;

public class OntologiePIPEngine extends StdConfigurableEngine {
	protected Log logger = LogFactory.getLog(this.getClass());

	public Collection<PIPRequest> attributesRequired() {
		Set<PIPRequest> requiredAttributes = new HashSet<PIPRequest>();
		return requiredAttributes;
	}

	public Collection<PIPRequest> attributesProvided() {
		Set<PIPRequest> attributes = new HashSet<PIPRequest>();
		return attributes;
	}

	public PIPResponse getAttributes(PIPRequest pipRequest, PIPFinder pipFinder) throws PIPException {

		 

		StdMutablePIPResponse mutablePIPResponse = new StdMutablePIPResponse();

		StdMutableAttribute stdMutableAttribute = new StdMutableAttribute();
		stdMutableAttribute.setAttributeId(pipRequest.getAttributeId());
		stdMutableAttribute.setCategory(pipRequest.getCategory());
		stdMutableAttribute.setIssuer(pipRequest.getIssuer());

		if (pipRequest.getCategory().toString()
				.equals("urn:oasis:names:tc:xacml:1.0:subject-category:access-subject")) {
			StdAttributeValue<?> staattributeValue = new StdAttributeValue(pipRequest.getDataTypeId(), "Julius Hibberts",
					null);
			stdMutableAttribute.addValue(staattributeValue);
		}

		if (pipRequest.getCategory().toString().equals("urn:oasis:names:tc:xacml:3.0:attribute-category:resource")) {
			StdAttributeValue<?> staattributeValue = new StdAttributeValue(pipRequest.getDataTypeId(),
					"http://medico.com/record/patient/BartSimpson", null);
			stdMutableAttribute.addValue(staattributeValue);
		}

		if (pipRequest.getCategory().toString().equals("urn:oasis:names:tc:xacml:3.0:attribute-category:action")) {
			StdAttributeValue<?> staattributeValue = new StdAttributeValue(pipRequest.getDataTypeId(), "read", null);
			stdMutableAttribute.addValue(staattributeValue);
		}

		mutablePIPResponse.addAttribute(stdMutableAttribute);

		return new StdSinglePIPResponse(stdMutableAttribute);

	}

}
