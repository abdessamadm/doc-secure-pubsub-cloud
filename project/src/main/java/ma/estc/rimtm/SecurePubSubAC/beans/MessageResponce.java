package ma.estc.rimtm.SecurePubSubAC.beans;

public class MessageResponce {

	private Boolean ok=true;
	private String message;
	private Integer errorCode;
	
	public MessageResponce(String lString) {
		this.message=lString;
	}
	
	public MessageResponce(Integer integer) {
		this.errorCode=integer;
	}
	
	public Boolean getOk() {
		return ok;
	}
	public void setOk(Boolean ok) {
		this.ok = ok;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	
}
