package ma.estc.rimtm.SecurePubSubAC.core;

import java.io.InputStream;

import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;


public class KerberosHttpClient {
	
    
	public HttpUriRequest getRequest(String url, int method,InputStream inputStream) {
		HttpUriRequest request =  new HttpPost(url);
        switch (method) {
		case 1:{
			request = new HttpGet(url);
		}break;
		case 2:{
			request = new HttpPut(url);
        	if(inputStream!=null) {
        		EntityBuilder builder=EntityBuilder.create();
                builder.setStream(inputStream);
                ((HttpPut)request).setEntity(builder.build());
            }
		}break;
		case 3:{
			request = new HttpDelete(url);
		}break;
		default:
			break;
		}
        
        return request;
        
	}

}