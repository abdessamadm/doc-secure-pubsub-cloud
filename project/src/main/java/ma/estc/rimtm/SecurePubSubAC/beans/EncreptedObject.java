package ma.estc.rimtm.SecurePubSubAC.beans;

import java.io.Serializable;

public class EncreptedObject implements Serializable {

	private byte[] key;
	private byte[] object;

	public byte[] getKey() {
		return key;
	}

	public void setKey(byte[] key) {
		this.key = key;
	}

	public byte[] getObject() {
		return object;
	}

	public void setObject(byte[] object) {
		this.object = object;
	}

}
