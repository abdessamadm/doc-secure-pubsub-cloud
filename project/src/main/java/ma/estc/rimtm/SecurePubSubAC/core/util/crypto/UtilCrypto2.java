package ma.estc.rimtm.SecurePubSubAC.core.util.crypto;
//package ma.estc.rimtm.SecurePubSub.core.util.crypto;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.DataInputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.ObjectInput;
//import java.io.ObjectInputStream;
//import java.io.ObjectOutput;
//import java.io.ObjectOutputStream;
//import java.math.BigInteger;
//import java.security.KeyFactory;
//import java.security.KeyStore;
//import java.security.KeyStoreException;
//import java.security.NoSuchAlgorithmException;
//import java.security.PrivateKey;
//import java.security.PublicKey;
//import java.security.SecureRandom;
//import java.security.UnrecoverableKeyException;
//import java.security.cert.Certificate;
//import java.security.cert.CertificateException;
//import java.security.cert.CertificateFactory;
//import java.security.cert.X509Certificate;
//import java.security.spec.InvalidKeySpecException;
//import java.security.spec.PKCS8EncodedKeySpec;
//
//import javax.crypto.Cipher;
//import javax.crypto.spec.IvParameterSpec;
//import javax.crypto.spec.SecretKeySpec;
//
//import android.os.Environment;
//
//public class UtilCrypto2 {
//
//	private static String KEY_STORE_DIRECTORY_LOCATION = UtilConstantes.getString("KEY_STORE_DIRECTORY_LOCATION");
//	private static String KEY_STORE_FILE_NAME = UtilConstantes.getString("KEY_STORE_FILE_NAME");
//
//	public static boolean isKeyStoreExiste() {
//
//		File file = new File(getKeyStoreDerectory());
//		if (file.exists()) {
//			file = new File(getKeyStorePath());
//			if (file.exists()) {
//				return true;
//			} else {
//				return false;
//			}
//		} else {
//			file.mkdirs();
//			return false;
//		}
//	}
//
//	public static void createNewStore(String pass) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
//		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
//		char[] password = pass.toCharArray();
//		ks.load(null, password);
//		FileOutputStream fos = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + KEY_STORE_DIRECTORY_LOCATION + "/" + KEY_STORE_FILE_NAME);
//		ks.store(fos, password);
//		fos.close();
//	}
//
//	public static KeyStore loadKeyStore(String pass) throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
//		FileInputStream is = new FileInputStream(getKeyStorePath());
//		KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
//		keystore.load(is, pass.toCharArray());
//		is.close();
//		return keystore;
//	}
//
//	public static void updateKeyStore(String pass, KeyStore keyStore) throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
//		char[] password = pass.toCharArray();
//		FileOutputStream out = new FileOutputStream(getKeyStorePath());
//		keyStore.store(out, password);
//		out.close();
//	}
//
//	public static boolean addX509ToKeyStore(String passe, String certfile, String alias) {
//		try {
//
//			KeyStore keystore = loadKeyStore(passe);
//			CertificateFactory cf = CertificateFactory.getInstance("X.509");
//			InputStream certstream = fullStream(certfile);
//			Certificate certs = cf.generateCertificate(certstream);
//			keystore.setCertificateEntry(alias, certs);
//			updateKeyStore(passe, keystore);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//
//	}
//	
//
//	
//	public static boolean deleteAlise(String passe, String alias) {
//		try {
//			KeyStore keystore = loadKeyStore(passe);
//			keystore.deleteEntry(alias);
//			updateKeyStore(passe, keystore);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return true;
//		}
//
//	}
//	
//	 
//
//
//	public static boolean addX509ToKeyStore(String passe, Certificate certfile, String alias) {
//		try {
//
//			KeyStore keystore = loadKeyStore(passe);
//			keystore.setCertificateEntry(alias, certfile);
//			updateKeyStore(passe, keystore);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return true;
//		}
//
//	}
//
//	private static InputStream fullStream(String fname) throws IOException {
//		FileInputStream fis = new FileInputStream(fname);
//		DataInputStream dis = new DataInputStream(fis);
//		byte[] bytes = new byte[dis.available()];
//		dis.readFully(bytes);
//		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
//		return bais;
//	}
//
//	public static String getKeyStorePath() {
//		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + KEY_STORE_DIRECTORY_LOCATION + "/" + KEY_STORE_FILE_NAME;
//	}
//
//	public static String getKeyStoreDerectory() {
//		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + KEY_STORE_DIRECTORY_LOCATION;
//	}
//
//	public static Certificate getX509ToKeyStore(String passe, String alias) {
//		try {
//			KeyStore keystore = loadKeyStore(passe);
//			Certificate certs = keystore.getCertificate(alias);
//			return certs;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//
//	}
//
//	public static String getLDAPValue(String allValue, String valueName) {
//		String[] s = allValue.split(",");
//		String returnval = "";
//		for (int i = 0; i < s.length; i++) {
//			System.out.println(s[i]);
//			if (s[i].contains(valueName)) {
//				returnval = s[i].replace(valueName + "=", "");
//				break;
//			}
//		}
//
//		return returnval;
//	}
//
//	public static void addPrivateKet(String pass, String aliasCertifica, String aliaceKey, String filename) throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, InvalidKeySpecException,Exception {
//		KeyStore ks = loadKeyStore(pass);
//		Certificate[] certArray = new Certificate[1];
//		certArray[0] = getX509ToKeyStore(pass, aliasCertifica);
//
//		File f = new File(filename);
//		FileInputStream fis = new FileInputStream(f);
//		DataInputStream dis = new DataInputStream(fis);
//		byte[] keyBytes = new byte[(int) f.length()];
//		dis.readFully(keyBytes);
//		dis.close();
//
//		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
//		KeyFactory kf = KeyFactory.getInstance("RSA");
//		PrivateKey key = kf.generatePrivate(spec);
//
//		boolean isValid=verifyKey(key,(X509Certificate)certArray[0]);
//		if(isValid){
//			ks.setKeyEntry(aliaceKey, key, pass.toCharArray(), certArray);
//			updateKeyStore(pass, ks);
//		}else {
//			throw new Exception("Rong ket");
//		}
//		
//		
//
//	}
// 
//	private static boolean verifyKey(PrivateKey key,X509Certificate aliasCertifica) {
//		String s="dalam";
//		try {
//			return new String(decrypt(encrypt(s.getBytes(), aliasCertifica.getPublicKey()),key)).equals(s);
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public static void addPrivateKet(  String pass, String aliasCertifica, String aliaceKey, PrivateKey key) throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, InvalidKeySpecException ,Exception{
//		KeyStore ks = loadKeyStore(pass);
//		Certificate[] certArray = new Certificate[1];
//		certArray[0] = getX509ToKeyStore(pass, aliasCertifica);
//		boolean isValid=verifyKey(key,(X509Certificate)certArray[0]);
//		if(isValid){
//			ks.setKeyEntry(aliaceKey, key, pass.toCharArray(), certArray);
//			updateKeyStore(pass, ks);
//		}else {
//			throw new Exception("Rong ket");
//		}
//		
//		
//
//	}
//
//	public static boolean getPrivatekey(String pass, String aliaceKey) throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, InvalidKeySpecException, UnrecoverableKeyException {
//		KeyStore ks = loadKeyStore(pass);
//
//		PrivateKey key = (PrivateKey) ks.getKey(aliaceKey, pass.toCharArray());
//
//		return key == null ? false : true;
//
//	}
//
//	//getPrivatekey2
//	public static boolean isPrivateKeyInKeyStore(String pPass, String pAliaceKey) {
//		KeyStore ks;
//		boolean retu = false;
//		try {
//			retu = getPrivateKeyFromKeyStore(pPass, pAliaceKey) == null ? false : true;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return retu;
//	}
//
//	//getPrivatekey1
//	public static PrivateKey getPrivateKeyFromKeyStore(String pPass, String pAliaceKey) throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, InvalidKeySpecException, UnrecoverableKeyException {
//		KeyStore ks = loadKeyStore(pPass);
//		return (PrivateKey) ks.getKey(pAliaceKey, pPass.toCharArray());
//	}
//
//	public static byte[] encrypt(byte[] text, PublicKey key) {
//		byte[] cipherText = null;
//		try {
//			final Cipher cipher = Cipher.getInstance("RSA");
//			cipher.init(Cipher.ENCRYPT_MODE, key);
//			cipherText = cipher.doFinal(text);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return cipherText;
//	}
//
//	public static byte[] decrypt(byte[] text, PrivateKey key) {
//		byte[] dectyptedText = null;
//		try {
//			final Cipher cipher = Cipher.getInstance("RSA");
//			cipher.init(Cipher.DECRYPT_MODE, key);
//			dectyptedText = cipher.doFinal(text);
//
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//
//		return dectyptedText;
//	}
//
//	public static byte[] getByteOfObject(Object object) {
//		ByteArrayOutputStream bos = new ByteArrayOutputStream();
//		ObjectOutput out = null;
//		byte[] yourBytes = null;
//		try {
//
//			out = new ObjectOutputStream(bos);
//			out.writeObject(object);
//			yourBytes = bos.toByteArray();
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			try {
//				if (out != null) {
//					out.close();
//				}
//			} catch (IOException ex) {
//				// ignore close exception
//			}
//			try {
//				bos.close();
//			} catch (IOException ex) {
//				// ignore close exception
//			}
//		}
//
//		return yourBytes;
//	}
//
//	public static Object getObjectFromByte(byte[] yourBytes) {
//		ByteArrayInputStream bis = new ByteArrayInputStream(yourBytes);
//		ObjectInput in = null;
//		Object o = null;
//		try {
//
//			in = new ObjectInputStream(bis);
//
//			o = in.readObject();
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			try {
//				bis.close();
//			} catch (IOException ex) {
//				// ignore close exception
//			}
//			try {
//				if (in != null) {
//					in.close();
//				}
//			} catch (IOException ex) {
//				// ignore close exception
//			}
//		}
//		return o;
//	}
//
//	static String IV = "AAAAAAAAAAAAAAAA";
//
//	public static byte[] encryptSymetrique(byte[] plainText, String encryptionKey)   {
//		byte[] b = null;
//		try {
//			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
//			SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
//			cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
//			return cipher.doFinal(plainText);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return b;
//
//	}
//
//	public static byte[] decryptSymetrique(byte[] cipherText, String encryptionKey)   {
//		byte[] b = null;
//		try {
//			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
//			SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
//			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
//			return cipher.doFinal(cipherText);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return b;
//	}
//	
//	public static String getSecureRandomString()   {
//		 SecureRandom random = new SecureRandom();
//		 String s= new BigInteger(130, random).toString(32);
//		 return s.substring(0, 16);
//		 
//	}
//	
//	  
//}
