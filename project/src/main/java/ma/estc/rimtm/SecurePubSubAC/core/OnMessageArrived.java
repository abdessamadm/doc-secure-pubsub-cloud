package ma.estc.rimtm.SecurePubSubAC.core;

public interface OnMessageArrived {

	public void OnMessageArrived(String pTopic,byte[] pPlayLoad);
}
