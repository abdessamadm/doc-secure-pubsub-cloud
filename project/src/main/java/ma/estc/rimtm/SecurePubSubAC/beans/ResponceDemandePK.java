package ma.estc.rimtm.SecurePubSubAC.beans;

import java.io.Serializable;
import java.security.cert.X509Certificate;

public class ResponceDemandePK implements DetailMessage, Serializable {

	private String topic;
	private X509Certificate topicCertificaCertificate;

	public X509Certificate getTopicCertificaCertificate() {
		return topicCertificaCertificate;
	}

	public void setTopicCertificaCertificate(
			X509Certificate topicCertificaCertificate) {
		this.topicCertificaCertificate = topicCertificaCertificate;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

}
