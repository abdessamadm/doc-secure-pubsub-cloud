package ma.estc.rimtm.SecurePubSubAC.pubsub.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import ma.estc.rimtm.SecurePubSubAC.core.OnMessageArrived;
import ma.estc.rimtm.SecurePubSubAC.pubsub.PublishSubcripe;

public class MqttPublishSubcripeImpl implements PublishSubcripe ,MqttCallback{
	
	private Integer qos      = 2;
	private String broker;
	private String clientId     ;
	private MemoryPersistence persistence = new MemoryPersistence();
	private MqttClient mClient ;
	private OnMessageArrived pOnMessageArrived;
	
	
	public MqttPublishSubcripeImpl(String broker, String clientId) {
		super();
		this.broker = broker;
		this.clientId = clientId;
	}

	
	public boolean publish(final byte[] pMessage, final String pTopic) {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				System.out.print("publish  "+pTopic+"  ...");
				try {
			    	 MqttMessage message = new MqttMessage(pMessage);
				     message.setQos(qos);
					 mClient.publish(pTopic, message);
					 System.out.println(" ok");
				} catch (MqttPersistenceException e) {
					e.printStackTrace();
				} catch (MqttException e) {
					e.printStackTrace();
				}
				
			}
		}).start();
		return true;
	}

	public void subcripe(String pTopics[]) {
		for(String lTopic:pTopics) {
			subcripe(lTopic);
		}
	}
	
	public boolean subcripe(String topic) {
		 
		System.out.print(topic+" ");
		System.out.print(" ... ");
		
		boolean returnBool=false;
		try {
			mClient.subscribe(topic);
			returnBool=true;
			System.out.println("  ok ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnBool;
	}

	public Boolean init() {
		System.out.print("connection "+clientId+" to "+broker+" ... ");
		boolean returnBool=false;
		try {
			mClient = new MqttClient(broker, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
	        connOpts.setCleanSession(true);
	        mClient.connect(connOpts);
	        returnBool=true;
	        System.out.println("ok");
	        mClient.setCallback(this);
		} catch (MqttException e) {
			e.printStackTrace();
		}
		return returnBool;
        
    }

	public void setOnMessageArrived(OnMessageArrived pOnMessageArrived) {
		this.pOnMessageArrived=pOnMessageArrived;
	}


	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub
		
	}


	public void messageArrived(final String topic, final MqttMessage message) throws Exception {
		//System.out.println("----------->"+topic);
new Thread(new Runnable() {
			
			@Override
			public void run() {
				pOnMessageArrived.OnMessageArrived(topic, message.getPayload());
				
			}
		}).start();
		
		
	}


	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void unsubcripe(String topic) {
		System.out.print(" unsubcripe "+topic+"  ");
		System.out.print(" ... ");
		
		 
		try {
			mClient.unsubscribe(topic);
		
			System.out.println("  ok ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
