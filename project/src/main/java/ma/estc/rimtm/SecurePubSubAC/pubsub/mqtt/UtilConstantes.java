package ma.estc.rimtm.SecurePubSubAC.pubsub.mqtt;

import java.util.ResourceBundle;

public class UtilConstantes {

	public static ResourceBundle constantes;
	
	public UtilConstantes() {
		constantes = ResourceBundle
				.getBundle("ma.estc.rimtm.SecurePubSub.resource.constantes");
	}

	public  static String getString(String key) {
		return constantes.getString(key);
	}
}