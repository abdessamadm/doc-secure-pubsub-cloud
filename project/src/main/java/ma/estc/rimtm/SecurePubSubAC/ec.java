package ma.estc.rimtm.SecurePubSubAC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.estc.rimtm.SecurePubSubAC.core.OnMessageArrivedControle;
import ma.estc.rimtm.SecurePubSubAC.core.RequesTraitement;
import ma.estc.rimtm.SecurePubSubAC.core.RequestTraitmentEventPublishImpl;
import ma.estc.rimtm.SecurePubSubAC.core.util.Constantes;
import ma.estc.rimtm.SecurePubSubAC.core.util.Utils;
import ma.estc.rimtm.SecurePubSubAC.pubsub.mqtt.MqttPublishSubcripeImpl;



public class ec {
//	-Dxacml.properties=testsets/conformance/xacml.properties
//	-Dlog4j.configuration=.\logging.properties
	
	
	
	public static void main(String[] args) throws Exception {
	//	new ec().run("Julius Hibbert" ,"http://medico.com/record/patient/BartSimpson" ,	"read");
	//	RequesTraitement requesTraitement=new RequesTraitement("ma.estc.rimtm.SecurePubSub.resource.ec");
		
		Constantes constantes=new Constantes();
    	Map<String , String> map=new HashMap<String, String>();
    	
    	map.put("CLIENT_ID", "EC");
    	map.put("STORE_KEY", "aaaa");
    	map.put("STORE_PATH", "/Users/absessamadmektoubi/git/doc-secure-pubsub-cloud/project/src/main/resources/cle/KeyStore/ec/KeyStore");
    	map.put("XACML_PATH", "/Users/absessamadmektoubi/git/doc-secure-pubsub-cloud/project/src/main/resources/testsets/conformance/xacml3.0-ct-v.0.4");
    	map.put("BROKER", "tcp://127.0.0.1:1883");
    	map.put("AT", "true");
    	
    	constantes.setMap(map);
    	
    	List<String> lListopic=new ArrayList<String>();
    	
    	
    	RequesTraitement requesTraitement=new RequesTraitement(new RequestTraitmentEventPublishImpl(map.get("BROKER"),map.get("CLIENT_ID")),constantes,lListopic,map.get("XACML_PATH"));
    	
    	MqttPublishSubcripeImpl clientMqtt=new MqttPublishSubcripeImpl(map.get("BROKER"), Utils.getRondom20());
    	clientMqtt.setOnMessageArrived(new OnMessageArrivedControle(requesTraitement));
    	clientMqtt.init();
    	clientMqtt.subcripe(map.get("CLIENT_ID")+"_controle");
	}
	
	
}
