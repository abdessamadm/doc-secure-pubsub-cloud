package ma.estc.rimtm.SecurePubSubAC.beans;

import java.io.Serializable;

public class Message implements Serializable {

	public static final int DEMANDE_PUBLIC_KEY = 1;
	public static final int RESPONCE_PUBLIC_KEY = 2;
	public static final int DEMANDE_PRIVATE_KEY = 3;
	public static final int RESPONCE_PRIVATE_KEY = 4;
	public static final int SIMPLE_MESSAGE = 5;
	public static final int ENCREPTED_MESSAGE = 6;
	public static final int DEMANDE_AUTORISATION_PRIVATE_KEY = 7;
	public static final int RESPONCE_AUTORISATION_PRIVATE_KEY = 8;
	
	
	private String client;
	private int typeMessage;
	private DetailMessage detailMessage;

	
	
	

	public Message(String client, int typeMessage, DetailMessage detailMessage) {
		super();
		this.client = client;
		this.typeMessage = typeMessage;
		this.detailMessage = detailMessage;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public int getTypeMessage() {
		return typeMessage;
	}

	public void setTypeMessage(int typeMessage) {
		this.typeMessage = typeMessage;
	}

	public DetailMessage getDetailMessage() {
		return detailMessage;
	}

	public void setDetailMessage(DetailMessage detailMessage) {
		this.detailMessage = detailMessage;
	}

}
