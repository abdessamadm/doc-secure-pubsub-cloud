package ma.estc.rimtm.SecurePubSubAC.beans;

import java.io.Serializable;

public class MessageEncrepted implements DetailMessage, Serializable {

	EncreptedObject encreptedObject;

	public EncreptedObject getEncreptedObject() {
		return encreptedObject;
	}

	public void setEncreptedObject(EncreptedObject encreptedObject) {
		this.encreptedObject = encreptedObject;
	}

}
