package ma.estc.rimtm.SecurePubSubAC.beans;

import java.io.Serializable;

public class TestMessage implements DetailMessage, Serializable {

	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
