package ma.estc.rimtm.SecurePubSubAC.xacml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.att.research.xacml.api.XACML3;
import com.att.research.xacml.std.StdMutableRequest;
import com.att.research.xacml.std.dom.DOMProperties;
import com.att.research.xacml.std.dom.DOMRequest;
import com.att.research.xacml.std.dom.DOMRequestAttributes;
import com.att.research.xacml.std.dom.DOMRequestDefaults;
import com.att.research.xacml.std.dom.DOMRequestReference;
import com.att.research.xacml.std.dom.DOMStructureException;
import com.att.research.xacml.std.dom.DOMUtil;

public class DOMRequestAC extends DOMRequest{

	public static StdMutableRequest load(String xmlString) throws DOMStructureException {
		StdMutableRequest request = null;
		InputStream is = null;
		try {
			is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));
			request = DOMRequestAC.load(is);
		} catch (UnsupportedEncodingException ex) {
			throw new DOMStructureException("Exception loading String Request: " + ex.getMessage(), ex);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch(Exception idontcare) {				
			}
		}
		return request;
	}
	
	/**
	 * Read a file containing the XML description of a XACML Request and parse it into a {@link com.att.research.xacml.api.Request} Object.
	 * 
	 * This is only used for testing.  
	 * In normal operation a Request arrives through the RESTful interface and is processed using <code>load(String xmlString)</code>.
	 * 
	 * @param fileRequest
	 * @return
	 * @throws DOMStructureException
	 */
	public static StdMutableRequest load(File fileRequest) throws DOMStructureException {
		StdMutableRequest request = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileRequest);
			request = DOMRequestAC.load(fis);
		} catch (FileNotFoundException ex) {
			throw new DOMStructureException("Exception loading File Request: " + ex.getMessage(), ex);
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch(Exception idontcare) {				
			}
		}
		return request;
	}
	
	/**
	 * Read characters from the given <code>InputStream</code> and parse them into an XACML {@link com.att.research.xacml.api.Request} object.
	 * 
	 * @param is
	 * @return
	 * @throws DOMStructureException
	 */
	public static StdMutableRequest load(InputStream is) throws DOMStructureException {
		/*
		 * Get the DocumentBuilderFactory
		 */
		DocumentBuilderFactory documentBuilderFactory	= DocumentBuilderFactory.newInstance();
		if (documentBuilderFactory == null) {
			throw new DOMStructureException("No XML DocumentBuilderFactory configured");
		}
		documentBuilderFactory.setNamespaceAware(true);
		
		/*
		 * Get the DocumentBuilder
		 */
		DocumentBuilder documentBuilder	= null;
		try {
			documentBuilder	= documentBuilderFactory.newDocumentBuilder();
		} catch (Exception ex) {
			throw new DOMStructureException("Exception creating DocumentBuilder: " + ex.getMessage(), ex);
		}
		
		/*
		 * Parse the XML file
		 */
		Document document	= null;
		StdMutableRequest request	= null;
		try {
			document	= documentBuilder.parse(is);
			if (document == null) {
				throw new Exception("Null document returned");
			}
			
			Node rootNode	= document.getFirstChild();
			if (rootNode == null) {
				throw new Exception("No child in document");
			}
			
			if (DOMUtil.isInNamespace(rootNode, XACML3.XMLNS)) {
				if (XACML3.ELEMENT_REQUEST.equals(rootNode.getLocalName())) {
					request	= DOMRequestAC.newInstance(rootNode);
					if (request == null) {
						throw new DOMStructureException("Failed to parse Request");
					}
				} else {
					throw DOMUtil.newUnexpectedElementException(rootNode);
				}
			} else {
				throw DOMUtil.newUnexpectedElementException(rootNode);
			}
		} catch (Exception ex) {
			throw new DOMStructureException("Exception loading Request: " + ex.getMessage(), ex);
		}
		return request;
	}
	
	/**
	 * Creates a new {@link com.att.research.xacml.api.Request} by parsing the given <code>Node</code> representing a XACML Request element.
	 * 
	 * @param nodeRequest the <code>Node</code> representing the XACML Request element.
	 * @return a new {@link com.att.research.xacml.std.StdMutableRequest} parsed from the given <code>Node</code>
	 * @throws DOMStructureException if the conversion cannot be made
	 */
	public static StdMutableRequest newInstance(Node nodeRequest) throws DOMStructureException {
		Element	elementRequest	= DOMUtil.getElement(nodeRequest);
		boolean bLenient		= DOMProperties.isLenient();
		
		StdMutableRequest stdMutableRequest	= new StdMutableRequest();

		stdMutableRequest.setReturnPolicyIdList(DOMUtil.getBooleanAttribute(elementRequest, XACML3.ATTRIBUTE_RETURNPOLICYIDLIST, !bLenient));
		stdMutableRequest.setCombinedDecision(DOMUtil.getBooleanAttribute(elementRequest, XACML3.ATTRIBUTE_COMBINEDDECISION, !bLenient));
		
		NodeList children	= elementRequest.getChildNodes();
		int numChildren;
		boolean sawAttributes	= false;
		if (children != null && (numChildren = children.getLength()) > 0) {
			for (int i = 0 ; i < numChildren ; i++) {
				Node child	= children.item(i);
				if (DOMUtil.isElement(child)) {
					if (DOMUtil.isInNamespace(child, XACML3.XMLNS)) {
						String childName	= child.getLocalName();
						if (XACML3.ELEMENT_ATTRIBUTES.equals(childName)) {
							stdMutableRequest.add(DOMRequestAttributes.newInstance(child));
							sawAttributes	= true;
						} else if (XACML3.ELEMENT_REQUESTDEFAULTS.equals(childName)) {
							stdMutableRequest.setRequestDefaults(DOMRequestDefaults.newInstance(child));
						} else if (XACML3.ELEMENT_MULTIREQUESTS.equals(childName)) {
							NodeList grandchildren	= child.getChildNodes();
							int numGrandchildren;
							if (grandchildren != null && (numGrandchildren = grandchildren.getLength()) > 0) {
								for (int j = 0 ; j < numGrandchildren ; j++) {
									Node grandchild	= grandchildren.item(j);
									if (DOMUtil.isElement(grandchild)) {
										if (DOMUtil.isInNamespace(grandchild, XACML3.XMLNS)) {
											if (XACML3.ELEMENT_REQUESTREFERENCE.equals(grandchild.getLocalName())) {
												stdMutableRequest.add(DOMRequestReference.newInstance(grandchild));												
											} else {
												if (!bLenient) {
													throw DOMUtil.newUnexpectedElementException(grandchild, nodeRequest);
												}
											}
										} else {
											if (!bLenient) {
												throw DOMUtil.newUnexpectedElementException(grandchild, nodeRequest);
											}
										}
									}
								}
							}
						} else {
							if (!bLenient) {
								throw DOMUtil.newUnexpectedElementException(child, nodeRequest);
							}
						}
					} else {
						if (!bLenient) {
							throw DOMUtil.newUnexpectedElementException(child, nodeRequest);
						}
					}
				}
			}
		}
		if (!sawAttributes && !bLenient) {
			throw DOMUtil.newMissingElementException(nodeRequest, XACML3.XMLNS, XACML3.ELEMENT_ATTRIBUTES);
		}
		
		return stdMutableRequest;
	}
}
