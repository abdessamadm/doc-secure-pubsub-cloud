package ma.estc.rimtm.SecurePubSubAC.core;

import java.util.Map;

import com.att.research.xacml.api.Decision;

import ma.estc.rimtm.SecurePubSubAC.beans.DemandePK;
import ma.estc.rimtm.SecurePubSubAC.beans.DemandeSK;
import ma.estc.rimtm.SecurePubSubAC.beans.Message;
import ma.estc.rimtm.SecurePubSubAC.beans.MessageSimple;
import ma.estc.rimtm.SecurePubSubAC.beans.ResponceDemandePK;
import ma.estc.rimtm.SecurePubSubAC.beans.ResponceDemandeSK;

public interface RequestTraitmentEvent {
	public void onSimpleMessageArrived(MessageSimple pMessageSimple,String pTopic);
	public void onEncreptedMessageArrived(MessageSimple pMessageSimple, String pTopic,boolean isDecrepted);
	public void onDemandePublickeyArrived(DemandePK lDemandePK, Message lMessageResponce, String pTopic, boolean isTraitement);
	public void onResponcePublickeyArrived(ResponceDemandePK pResponceDemandePK, String pTopic, boolean isTraitement);
	public void onDemandePrivateKeyArrived(DemandeSK lDemandeSK, ResponceDemandeSK lResponceDemandeSK,boolean isTraitement);
	public void onResponcePrivateKeyArrived(ResponceDemandeSK lResponDemandeSK, boolean b);
	public void onRun(Map<String,String> pParams);
	public void onSendDemandePublicKey(String Topic);
	public void OnsendDemandeAutorisation(String pTopic);
	public void readDemandeAutorisation(String User, String topic, String operation, Decision reponce);
	public void OnReadResponceDemandeAutorisation(String topic);
	public void onSendDemandeSecreteKey(String pTopic);
	public void onSendEncreptedMessage(String pTopic, String mesage);
}
