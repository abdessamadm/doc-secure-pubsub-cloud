package ma.estc.rimtm.SecurePubSubAC.core;

import java.io.File;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.att.research.xacml.api.Decision;
import com.att.research.xacml.api.Request;
import com.att.research.xacml.api.Response;
import com.att.research.xacml.api.Result;
import com.att.research.xacml.api.pdp.PDPEngine;
import com.att.research.xacml.api.pdp.PDPEngineFactory;
import com.att.research.xacml.std.dom.DOMProperties;
import com.att.research.xacml.std.dom.DOMRequest;
import com.att.research.xacml.util.XACMLProperties;
import com.att.research.xacmlatt.pdp.std.StdPolicyFinderFactory;

import ma.estc.rimtm.SecurePubSubAC.beans.DemandePK;
import ma.estc.rimtm.SecurePubSubAC.beans.DemandeSK;
import ma.estc.rimtm.SecurePubSubAC.beans.DetailDemandeSK;
import ma.estc.rimtm.SecurePubSubAC.beans.EncreptedObject;
import ma.estc.rimtm.SecurePubSubAC.beans.Message;
import ma.estc.rimtm.SecurePubSubAC.beans.MessageEncrepted;
import ma.estc.rimtm.SecurePubSubAC.beans.MessageSimple;
import ma.estc.rimtm.SecurePubSubAC.beans.ResponceDemandePK;
import ma.estc.rimtm.SecurePubSubAC.beans.ResponceDemandeSK;
import ma.estc.rimtm.SecurePubSubAC.beans.autorisation.DemandeAutorisationSK;
import ma.estc.rimtm.SecurePubSubAC.beans.autorisation.DetailAutorisationSK;
import ma.estc.rimtm.SecurePubSubAC.beans.autorisation.DetailResponceAutorisationCryptedSK;
import ma.estc.rimtm.SecurePubSubAC.beans.autorisation.DetailResponceAutorisationSK;
import ma.estc.rimtm.SecurePubSubAC.beans.autorisation.DetailResponceAutorisationSignierSK;
import ma.estc.rimtm.SecurePubSubAC.core.util.Constantes;
import ma.estc.rimtm.SecurePubSubAC.core.util.Utils;
import ma.estc.rimtm.SecurePubSubAC.core.util.crypto.UtilCrypto;
import ma.estc.rimtm.SecurePubSubAC.exception.SecurePubSubException;
import ma.estc.rimtm.SecurePubSubAC.pubsub.PublishSubcripe;
import ma.estc.rimtm.SecurePubSubAC.pubsub.mqtt.MqttPublishSubcripeImpl;
import ma.estc.rimtm.SecurePubSubAC.xacml.XacmlPEP;

public class RequesTraitement {

	private final String TOPIC_DEMANDE = "TOPIC_DEMANDE";
	private final String TOPIC_AUTORISATION_DEMANDE = "TOPIC_AUTORISATION_DEMANDE";
	private static final String ERROR_INIT_PUBLISH_SUBCRIPE = "ERROR_INIT_PUBLISH_SUBCRIPE";
	private static final String ALIAS_TOPIC_PRIVATE_KEY = "ALIAS_TOPIC_PRIVATE_KEY";
	private static final String ALIAS_TOPIC_CERTIF = "ALIAS_TOPIC_CERTIF";
	private static final String ALIAS_CLIENT_PRIVATE_KEY = "ALIAS_CLIENT_PRIVATE_KEY";
	private static final String ALIAS_CLIENT_CERTIF = "ALIAS_CLIENT_CERTIF";
	private static final String ALIAS_CA_CERTIF = "ALIAS_CA_CERTIF";
	private static final String ALIAS_EC_CERTIF = "ALIAS_EC_CERTIF";

	private String mClientId;
	private String mPasse;// = "aaaa";
	private String mStorePath;
	private String xacmlpath;
	private String mBroker;
	private UtilCrypto utilCrypto;
	private boolean isAutoriteDeConfiance;
	private PDPEngineFactory pdpEngineFactory;

	private PublishSubcripe mPublishSubcripe;
	public PublishSubcripe getmPublishSubcripe() {
		return mPublishSubcripe;
	}

	public void setmPublishSubcripe(PublishSubcripe mPublishSubcripe) {
		this.mPublishSubcripe = mPublishSubcripe;
	}

	private OnMessageArrived mOnMessageArrived;
	private RequestTraitmentEvent mRequestTraitmentEvent;
	private List<String> mListTopic=new ArrayList<String>();

	public RequesTraitement(RequestTraitmentEvent pRequestTraitmentEvent, Constantes pConstantes,List<String> pListTopic,String xacmlpath)
			throws SecurePubSubException {

		mClientId = pConstantes.getString("CLIENT_ID");
		mPasse = pConstantes.getString("STORE_KEY");
		mStorePath = pConstantes.getString("STORE_PATH");
		mBroker = pConstantes.getString("BROKER");
		isAutoriteDeConfiance = Boolean.valueOf(pConstantes.getString("AT"));
		utilCrypto = new UtilCrypto(mStorePath);
		this.mOnMessageArrived = new OnMessageArrivedImpl(this, mStorePath);
		mRequestTraitmentEvent = pRequestTraitmentEvent;
		mListTopic=pListTopic;
		init();
		mRequestTraitmentEvent.onRun(pConstantes.getMap());
		this.xacmlpath=xacmlpath;
	}

	public void init() throws SecurePubSubException {

		this.mPublishSubcripe = new MqttPublishSubcripeImpl(mBroker, mClientId);
		if (!mPublishSubcripe.init()) {
			throw new SecurePubSubException(ERROR_INIT_PUBLISH_SUBCRIPE);
		}

		mPublishSubcripe.subcripe(getTopicToSubscribe());
		mPublishSubcripe.setOnMessageArrived(mOnMessageArrived);
		checKeyStore();
	}

	public boolean isAutoriteDeConfiance() {
		return isAutoriteDeConfiance;
	}

	public void setAutoriteDeConfiance(boolean isAutoriteDeConfiance) {
		this.isAutoriteDeConfiance = isAutoriteDeConfiance;
	}

	private String[] getTopicToSubscribe() {
		List<String> lList = new ArrayList<String>();

		if (isAutoriteDeConfiance) {
			lList.add(TOPIC_AUTORISATION_DEMANDE);
		} else {
			lList.add(TOPIC_DEMANDE);
		}

		for (String topic : mListTopic) {
			lList.add(topic);
		}

		return Utils.listToArray(lList);

	}

	public String getClientId() {
		return mClientId;
	}

	private String getAliasTopicPrivateKey(String pTopic) {
		return pTopic + ALIAS_TOPIC_PRIVATE_KEY;
	}

	private String getAliasCertificate(String pTopic) {
		return pTopic + ALIAS_TOPIC_CERTIF;
	}

	private String getNewResponceTopic() {
		return "responce_" + mClientId + "_" + Utils.getRondom20() + "";
	}

	private void checKeyStore() {
		// TODO
	}

	public void readSimpleMessage(Message pMessage, String pTopic) {
		MessageSimple lMessageSimple = (MessageSimple) pMessage.getDetailMessage();
		mRequestTraitmentEvent.onSimpleMessageArrived(lMessageSimple, pTopic);
	}

	public void readEncreptedMessage(Message message, String pTopic) {

		if (utilCrypto.isPrivateKeyInKeyStore(mPasse, getAliasTopicPrivateKey(pTopic))) {
			MessageEncrepted messageEncrepted = (MessageEncrepted) message.getDetailMessage();
			try {
				MessageSimple lMessageSimple = (MessageSimple) utilCrypto.getDecreptedObject(
						messageEncrepted.getEncreptedObject(),
						utilCrypto.getPrivateKeyWithAlice(mPasse, getAliasTopicPrivateKey(pTopic)));
				mRequestTraitmentEvent.onEncreptedMessageArrived(lMessageSimple, pTopic, true);
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			mRequestTraitmentEvent.onEncreptedMessageArrived(null, pTopic, false);
		}
	}

	public void readDemandePk(Message pMessage) {

		DemandePK lDemandePK = (DemandePK) pMessage.getDetailMessage();
		if (utilCrypto.isX509FromKeyStore(mPasse, getAliasCertificate(lDemandePK.getTopic()))) {
			ResponceDemandePK responceDemandePK = new ResponceDemandePK();
			responceDemandePK.setTopic(lDemandePK.getTopic());
			responceDemandePK.setTopicCertificaCertificate((X509Certificate) utilCrypto.getX509FromKeyStore(mPasse,
					getAliasCertificate(lDemandePK.getTopic())));
			Message lMessageResponce = new Message(getClientId(), Message.RESPONCE_PUBLIC_KEY, responceDemandePK);
			mRequestTraitmentEvent.onDemandePublickeyArrived(lDemandePK, lMessageResponce, lDemandePK.getTopic(), true);
			mPublishSubcripe.publish(utilCrypto.getByteOfObject(lMessageResponce), lDemandePK.getResponceTopic());
			
		} else {
			mRequestTraitmentEvent.onDemandePublickeyArrived(lDemandePK, null, lDemandePK.getTopic(), false);
		}
	}

	public void readResponcePK(Message pMessage) {

		ResponceDemandePK pResponceDemandePK = (ResponceDemandePK) pMessage.getDetailMessage();
		if (pResponceDemandePK.getTopicCertificaCertificate() != null) {
			utilCrypto.addX509ToKeyStore(mPasse, pResponceDemandePK.getTopicCertificaCertificate(),
					getAliasCertificate(pResponceDemandePK.getTopic()));
			
			// TODO verifay certificate
			mRequestTraitmentEvent.onResponcePublickeyArrived(pResponceDemandePK, pResponceDemandePK.getTopic(), true);
			sendDemandeAutorisation(pResponceDemandePK.getTopic());
		}

	}

	public void readDemandeSK(Message message) {

		DemandeSK lDemandeSK = (DemandeSK) message.getDetailMessage();
		if (utilCrypto.isX509FromKeyStore(mPasse, getAliasCertificate(lDemandeSK.getTopic()))
				&& utilCrypto.isPrivateKeyInKeyStore(mPasse, getAliasTopicPrivateKey(lDemandeSK.getTopic()))) {
			DetailDemandeSK detailDemandeSK;
			try {
				PrivateKey lPrivateKey = utilCrypto.getPrivateKeyWithAlice(mPasse,
						getAliasTopicPrivateKey(lDemandeSK.getTopic()));
				detailDemandeSK = (DetailDemandeSK) utilCrypto.getDecreptedObject(lDemandeSK.getEncreptedMessage(),
						lPrivateKey);
				// TODO verifier signature
				DetailResponceAutorisationSignierSK lDetailResponceAutorisationSignierSK = detailDemandeSK
						.getDetailResponceAutorisationSignierSK();
				Decision s = (Decision) utilCrypto.getDecreptedObject(
						lDetailResponceAutorisationSignierSK.getDetailResponceAutorisationSK().getResponce(),
						lPrivateKey);

				System.out.println(s);

				if (s.toString().equals("Permit")) {
					ResponceDemandeSK lResponceDemandeSK = new ResponceDemandeSK();
					lResponceDemandeSK.setTopic(lDemandeSK.getTopic());
					EncreptedObject lEncreptedObject = utilCrypto.getEncreptedObject(lPrivateKey,
							lDetailResponceAutorisationSignierSK.getDetailResponceAutorisationSK()
									.getClientCertificate().getPublicKey());
					lResponceDemandeSK.setEncreptedMessage(lEncreptedObject);

					Message messageResponce = new Message(getClientId(), Message.RESPONCE_PRIVATE_KEY,
							lResponceDemandeSK);
					mPublishSubcripe.publish(utilCrypto.getByteOfObject(messageResponce),
							detailDemandeSK.getResponceTopic());
					mRequestTraitmentEvent.onDemandePrivateKeyArrived(lDemandeSK, lResponceDemandeSK, true);
				}else {
					mRequestTraitmentEvent.onDemandePrivateKeyArrived(lDemandeSK, null, false);
				}

			} catch (Exception e) {
				e.printStackTrace();
				mRequestTraitmentEvent.onDemandePrivateKeyArrived(lDemandeSK, null, false);
			}
		} else {
			mRequestTraitmentEvent.onDemandePrivateKeyArrived(lDemandeSK, null, false);
		}
	}

	public void readResponcSK(Message message) {
		ResponceDemandeSK lResponDemandeSK = (ResponceDemandeSK) message.getDetailMessage();
		try {
			PrivateKey key = (PrivateKey) utilCrypto.getDecreptedObject(lResponDemandeSK.getEncreptedMessage(),
					utilCrypto.getPrivateKeyWithAlice(mPasse, ALIAS_CLIENT_PRIVATE_KEY));
			if (key != null) {
				utilCrypto.addPrivateKet(mPasse, getAliasCertificate(lResponDemandeSK.getTopic()),
						getAliasTopicPrivateKey(lResponDemandeSK.getTopic()), key);
				mRequestTraitmentEvent.onResponcePrivateKeyArrived(lResponDemandeSK, true);
			}else {
				mRequestTraitmentEvent.onResponcePrivateKeyArrived(lResponDemandeSK, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean sendSimpleMessage(String pMessage, String pTopic) {
		MessageSimple messageSimple = new MessageSimple();
		messageSimple.setClinetName(getClientId());
		messageSimple.setText(pMessage);

		Message message = new Message(getClientId(), Message.SIMPLE_MESSAGE, messageSimple);

		return mPublishSubcripe.publish(utilCrypto.getByteOfObject(message), pTopic);
	}

	public boolean sendEncreptedMessage(String pMessage, String pTopic) {
		MessageSimple messageSimple = new MessageSimple();
		messageSimple.setClinetName(getClientId());
		messageSimple.setText(pMessage);

		EncreptedObject encreptedObject = utilCrypto.getEncreptedObject(messageSimple,
				utilCrypto.getX509FromKeyStore(mPasse, getAliasCertificate(pTopic)).getPublicKey());

		MessageEncrepted messageEncrepted = new MessageEncrepted();
		messageEncrepted.setEncreptedObject(encreptedObject);

		Message message = new Message(getClientId(), Message.ENCREPTED_MESSAGE, messageEncrepted);
		mRequestTraitmentEvent.onSendEncreptedMessage(pTopic,pMessage);
		return mPublishSubcripe.publish(utilCrypto.getByteOfObject(message), pTopic);
	}

	public boolean sendDemandeSecreteKey(String pTopic,
			DetailResponceAutorisationSignierSK lDetailResponceAutorisationSignierSK) {

		DetailDemandeSK detailDemandeSK = new DetailDemandeSK();
		detailDemandeSK.setResponceTopic(getNewResponceTopic());
		detailDemandeSK.setDetailResponceAutorisationSignierSK(lDetailResponceAutorisationSignierSK);

		EncreptedObject encreptedObject = utilCrypto.getEncreptedObject(detailDemandeSK,
				utilCrypto.getX509FromKeyStore(mPasse, getAliasCertificate(pTopic)).getPublicKey());

		DemandeSK demandePS = new DemandeSK();
		demandePS.setTopic(pTopic);
		demandePS.setEncreptedMessage(encreptedObject);

		Message message = new Message(getClientId(), Message.DEMANDE_PRIVATE_KEY, demandePS);
		mPublishSubcripe.subcripe(detailDemandeSK.getResponceTopic());
		mRequestTraitmentEvent.onSendDemandeSecreteKey(pTopic);
		return mPublishSubcripe.publish(utilCrypto.getByteOfObject(message), TOPIC_DEMANDE);
	}

	public boolean sendDemandePublicKey(String pTopic) {
		DemandePK demandePK = new DemandePK();
		demandePK.setResponceTopic(getNewResponceTopic());
		demandePK.setTopic(pTopic);

		Message message = new Message(getClientId(), Message.DEMANDE_PUBLIC_KEY, demandePK);
		mPublishSubcripe.subcripe(demandePK.getResponceTopic());
		mRequestTraitmentEvent.onSendDemandePublicKey(pTopic);
		return mPublishSubcripe.publish(utilCrypto.getByteOfObject(message), TOPIC_DEMANDE);
	}

	public boolean sendDemandeAutorisation(String pTopic) {

		DetailAutorisationSK detailAutorisationSK = new DetailAutorisationSK();
		detailAutorisationSK.setResponceTopic(getNewResponceTopic());
		detailAutorisationSK
				.setClientCertificate((X509Certificate) utilCrypto.getX509FromKeyStore(mPasse, ALIAS_CLIENT_CERTIF));

		EncreptedObject encreptedObject = utilCrypto.getEncreptedObject(detailAutorisationSK,
				utilCrypto.getX509FromKeyStore(mPasse, ALIAS_EC_CERTIF).getPublicKey());

		DemandeAutorisationSK demandeAutorisationSK = new DemandeAutorisationSK();
		demandeAutorisationSK.setTopic(pTopic);
		demandeAutorisationSK.setEncreptedMessage(encreptedObject);

		Message message = new Message(getClientId(), Message.DEMANDE_AUTORISATION_PRIVATE_KEY, demandeAutorisationSK);
		mRequestTraitmentEvent.OnsendDemandeAutorisation(pTopic);
		mPublishSubcripe.subcripe(detailAutorisationSK.getResponceTopic());
		return mPublishSubcripe.publish(utilCrypto.getByteOfObject(message), TOPIC_AUTORISATION_DEMANDE);
	}

	public void readDemandeAutorisation(Message message) {
		DemandeAutorisationSK lDemandeAutorisationSK = (DemandeAutorisationSK) message.getDetailMessage();

		try {

			DetailAutorisationSK detailAutorisationSK = (DetailAutorisationSK) utilCrypto.getDecreptedObject(
					lDemandeAutorisationSK.getEncreptedMessage(),
					utilCrypto.getPrivateKeyWithAlice(mPasse, ALIAS_CLIENT_PRIVATE_KEY));
			X509Certificate x509Certificate = detailAutorisationSK.getClientCertificate();
			
			Decision reponce = new XacmlPEP().run(x509Certificate.getSubjectDN().getName(),
					lDemandeAutorisationSK.getTopic(), "read",xacmlpath);

			DetailResponceAutorisationSK detailResponceAutorisationSK = new DetailResponceAutorisationSK();

			detailResponceAutorisationSK.setClientCertificate(detailAutorisationSK.getClientCertificate());
			detailResponceAutorisationSK.setTopic(lDemandeAutorisationSK.getTopic());
			detailResponceAutorisationSK.setResponce(utilCrypto.getEncreptedObject(reponce,
					utilCrypto.getX509FromKeyStore(mPasse, getAliasCertificate(lDemandeAutorisationSK.getTopic()))
							.getPublicKey()));

			DetailResponceAutorisationSignierSK detailResponceAutorisationSignierSK = new DetailResponceAutorisationSignierSK();
			detailResponceAutorisationSignierSK.setDetailResponceAutorisationSK(detailResponceAutorisationSK);
			detailResponceAutorisationSignierSK
					.setSignature(utilCrypto.sign(utilCrypto.getByteOfObject(detailResponceAutorisationSK),
							utilCrypto.getPrivateKeyWithAlice(mPasse, ALIAS_CLIENT_PRIVATE_KEY)));

			DetailResponceAutorisationCryptedSK detailResponceAutorisationCryptedSK = new DetailResponceAutorisationCryptedSK();
			detailResponceAutorisationCryptedSK.setDetailResponceAutorisationCryptedSK(utilCrypto.getEncreptedObject(
					detailResponceAutorisationSignierSK, detailAutorisationSK.getClientCertificate().getPublicKey()));

			Message messageResponce = new Message(getClientId(), Message.RESPONCE_AUTORISATION_PRIVATE_KEY,
					detailResponceAutorisationCryptedSK);
			
			mRequestTraitmentEvent.readDemandeAutorisation(x509Certificate.getSubjectDN().getName(),
					lDemandeAutorisationSK.getTopic(), "read",reponce);
			
			mPublishSubcripe.publish(utilCrypto.getByteOfObject(messageResponce),
					detailAutorisationSK.getResponceTopic());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void readResponceDemandeAutorisation(Message message) {
		try {
			DetailResponceAutorisationCryptedSK lDetailResponceAutorisationCryptedSK = (DetailResponceAutorisationCryptedSK) message
					.getDetailMessage();

			final DetailResponceAutorisationSignierSK responceAutorisationSignierSK = (DetailResponceAutorisationSignierSK) utilCrypto
					.getDecreptedObject(lDetailResponceAutorisationCryptedSK.getDetailResponceAutorisationCryptedSK(),
							utilCrypto.getPrivateKeyWithAlice(mPasse, ALIAS_CLIENT_PRIVATE_KEY));
			mRequestTraitmentEvent.OnReadResponceDemandeAutorisation(responceAutorisationSignierSK.getDetailResponceAutorisationSK().getTopic());
			sendDemandeSecreteKey(responceAutorisationSignierSK.getDetailResponceAutorisationSK().getTopic(),
							responceAutorisationSignierSK);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Decision run() throws Exception {

		Decision decision = null;
		PDPEngineFactory thisPDPEngineFactory = null;
		thisPDPEngineFactory = this.getPDPEngineFactory();
		Request request = null;
		DOMProperties.setLenient(true);
		request = DOMRequest.load(
				"C:\\Users\\Abdessamad\\Desktop\\securityPUBSUB\\SecurePubSubAC\\src\\main\\resources\\testsets\\conformance\\xacml3.0-ct-v.0.4\\IIA001Request.xml");
		setXACMLProperties(
				"C:\\Users\\Abdessamad\\Desktop\\securityPUBSUB\\SecurePubSubAC\\src\\main\\resources\\testsets\\conformance\\xacml3.0-ct-v.0.4\\IIA001Policy.xml");
		PDPEngine pdpEngine = null;
		pdpEngine = thisPDPEngineFactory.newEngine();
		Response response = pdpEngine.decide(request);
		Iterator<Result> s = response.getResults().iterator();
		while (s.hasNext()) {
			Result type = (Result) s.next();
			System.out.println(type.getDecision());
			decision = type.getDecision();
		}

		return decision;

	}

	protected PDPEngineFactory getPDPEngineFactory() throws Exception {

		if (this.pdpEngineFactory == null) {
			this.pdpEngineFactory = PDPEngineFactory.newInstance();
		}
		return this.pdpEngineFactory;
	}

	private void setXACMLProperty(String propertyName, File listFiles) {

		StringBuilder stringBuilderIdList = new StringBuilder();

		File file = listFiles;
		if (stringBuilderIdList.length() > 0) {
			stringBuilderIdList.append(',');
		}
		stringBuilderIdList.append(file.getName());

		XACMLProperties.setProperty(file.getName() + StdPolicyFinderFactory.PROP_FILE, file.getAbsolutePath());
		XACMLProperties.setProperty(propertyName, stringBuilderIdList.toString());
	}

	public void setXACMLProperties(String rootPolicies) {
		setXACMLProperty(XACMLProperties.PROP_ROOTPOLICIES, new File(rootPolicies));
	}

}
