package ma.estc.rimtm.SecurePubSubAC.beans;

import java.io.Serializable;

public class DemandeSK implements DetailMessage, Serializable {

	private String topic;
	private EncreptedObject encreptedMessage;

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public EncreptedObject getEncreptedMessage() {
		return encreptedMessage;
	}

	public void setEncreptedMessage(EncreptedObject encreptedMessage) {
		this.encreptedMessage = encreptedMessage;
	}

}
