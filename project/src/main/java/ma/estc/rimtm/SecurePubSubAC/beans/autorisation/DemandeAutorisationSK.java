package ma.estc.rimtm.SecurePubSubAC.beans.autorisation;

import java.io.Serializable;

import ma.estc.rimtm.SecurePubSubAC.beans.DetailMessage;
import ma.estc.rimtm.SecurePubSubAC.beans.EncreptedObject;

public class DemandeAutorisationSK implements DetailMessage, Serializable {

	private String topic;
	private EncreptedObject encreptedMessage;

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public EncreptedObject getEncreptedMessage() {
		return encreptedMessage;
	}

	public void setEncreptedMessage(EncreptedObject encreptedMessage) {
		this.encreptedMessage = encreptedMessage;
	}

}
