package ma.estc.rimtm.SecurePubSubAC.exception;

public class SecurePubSubException extends Exception{
	public SecurePubSubException(String pStringError) {
		super(pStringError);
	}

	private static final long serialVersionUID = 1L;
}
