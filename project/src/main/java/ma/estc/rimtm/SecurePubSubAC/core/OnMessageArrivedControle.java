package ma.estc.rimtm.SecurePubSubAC.core;

public class OnMessageArrivedControle implements OnMessageArrived{
	
	private RequesTraitement mRequesTraitement;
	private final int SEND_SIMPLE_MESSAGE=1;
	private final int SEND_ENCREPTED_MESSAGE=2;
	private final int SEND_PUBKEY_REQUEST=3;
	private final int SEND_SECKEY_REQUEST=4;
	private final int SEND_MESSAGES=5;
	private final int SUBSCREB_MESSAGES=6;
	
	public OnMessageArrivedControle(RequesTraitement pRequesTraitement) {
		this.mRequesTraitement=pRequesTraitement;
		
	}

	public void OnMessageArrived(String pTopic, byte[] pPlayLoad) {
		final String message = new String(pPlayLoad);
		final String[] messageTap = message.split(";");
		
		switch (Integer.valueOf(messageTap[0])) {
		case SEND_SIMPLE_MESSAGE:
			mRequesTraitement.sendSimpleMessage(messageTap[2], messageTap[1]);
			break;
		case SEND_ENCREPTED_MESSAGE:
			mRequesTraitement.sendEncreptedMessage(messageTap[2], messageTap[1]);
			break;
		case SEND_PUBKEY_REQUEST:
			mRequesTraitement.sendDemandePublicKey(messageTap[1]);
			break;
		case SEND_SECKEY_REQUEST:
			mRequesTraitement.sendDemandeAutorisation(messageTap[1]);
			break;
		case SEND_MESSAGES:
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					
					for (int i = 0; i < Integer.valueOf(messageTap[2]); i++) {
						mRequesTraitement.sendEncreptedMessage(messageTap[3], messageTap[1]);
					}
					
				}
			}).start();
			
			break;
		case SUBSCREB_MESSAGES:
			mRequesTraitement.getmPublishSubcripe().subcripe(messageTap[1]);
			break;
		default:
			break;
		}
		
		
	}

}
