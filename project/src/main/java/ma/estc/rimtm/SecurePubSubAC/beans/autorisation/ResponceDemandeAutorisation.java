package ma.estc.rimtm.SecurePubSubAC.beans.autorisation;

import java.io.Serializable;
import java.security.cert.X509Certificate;

import ma.estc.rimtm.SecurePubSubAC.beans.DetailMessage;
import ma.estc.rimtm.SecurePubSubAC.beans.EncreptedObject;

public class ResponceDemandeAutorisation implements DetailMessage, Serializable {
	private EncreptedObject encreptedMessage;
	private String topic;

	public EncreptedObject getEncreptedMessage() {
		return encreptedMessage;
	}

	public void setEncreptedMessage(EncreptedObject encreptedMessage) {
		this.encreptedMessage = encreptedMessage;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	

}
