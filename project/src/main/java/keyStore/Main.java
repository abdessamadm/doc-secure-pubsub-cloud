package keyStore;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.crypto.Cipher;
// avant d'exécuter ce program il faut crea l'ensemble des cle
public class Main {

	String pass="aaaa";
	//String dir="/home/abdessamad/Desktop/KeyStore/";
	String dir="/Users/absessamadmektoubi/git/doc-secure-pubsub-cloud/project/src/main/resources/cle/KeyStore/";
	
	public static void main(String[] args) {
		
		new Main().initUserStore("g1/ca/","ec/","g1/user1/","topic1","g1/topic1/");
		new Main().initUserStore("g1/ca/","ec/","g1/user2/",null,null);
		new Main().initUserStore("g2/ca/","ec/","g2/user1/","topic1","g2/topic1/");
		new Main().initUserStore("g2/ca/","ec/","g2/user2/",null,null);
		
	 	new Main().initUserEC("ec/", "g1/ca/", "g2/ca/","topic1","g1/topic1/");
		
		
		System.out.println("gneration keyStore ok ");
	}
	
	public   void initUserStore(String pthCA,String pthEC,String pth,String topicName,String pathTopic) {
		try {
			String pathCertifCA=dir+pthCA+"cert.pem";
			String aliasCertifCA="ALIAS_CA_CERTIF";
			
			String workingDir=dir+pth;
			
			 
			
			String dirStore=workingDir+"KeyStore";
			String pathCertif=workingDir+"cert.pem";
			String pathkey=workingDir+"key.pkcs8";
			String aliasCertif="ALIAS_CLIENT_CERTIF";
			String aliasKey="ALIAS_CLIENT_PRIVATE_KEY";
			
			String pathCertifEc=dir+pthEC+"cert.pem";
			String aliasCertifEc="ALIAS_EC_CERTIF";
			
			
			
			createKeyStore(pass,dirStore);
			addX509ToKeyStore(pass,pathCertifCA,dirStore,aliasCertifCA);
			addX509ToKeyStore(pass,pathCertif,dirStore,aliasCertif);
			addPrivateKet(pass, aliasCertif, aliasKey, pathkey,dirStore);
			
			if(null!=topicName) {
				String workingDirTopic=dir+pathTopic;
				String pathCertifTopic=workingDirTopic+"cert.pem";
				String pathkeyTopic=workingDirTopic+"key.pkcs8";
				String aliasCertifTopic=topicName+"ALIAS_TOPIC_CERTIF";
				String aliasKeyTopic=topicName+"ALIAS_TOPIC_PRIVATE_KEY";
				
				addX509ToKeyStore(pass,pathCertifTopic,dirStore,aliasCertifTopic);
				addPrivateKet(pass, aliasCertifTopic, aliasKeyTopic, pathkeyTopic,dirStore);
			}
			addX509ToKeyStore(pass,pathCertifEc,dirStore,aliasCertifEc);
		} catch (Exception e) {
			e.printStackTrace();
		}  
	}
	
	public   void initUserEC(String pth,String pthG1,String pthG2,String topicName,String pathTopic) {
		try {
			
			String pathCertifG1=dir+pthG1+"cert.pem";
			String pathCertifG2=dir+pthG2+"cert.pem";
			
			String workingDir=dir+pth;
			String dirStore=workingDir+"KeyStore";
			String pathCertif=workingDir+"cert.pem";
			String pathkey=workingDir+"key.pkcs8";
			String aliasCertif="ALIAS_CLIENT_CERTIF";
			String aliasKey="ALIAS_CLIENT_PRIVATE_KEY";
			
			
			createKeyStore(pass,dirStore);
			addX509ToKeyStore(pass,pathCertif,dirStore,aliasCertif);
			addX509ToKeyStore(pass,pathCertifG1,dirStore,"G1");
			addX509ToKeyStore(pass,pathCertifG2,dirStore,"G2");
			 
			addPrivateKet(pass, aliasCertif, aliasKey, pathkey,dirStore);
			

			if(null!=topicName) {
				String workingDirTopic=dir+pathTopic;
				String pathCertifTopic=workingDirTopic+"cert.pem";
				String pathkeyTopic=workingDirTopic+"key.pkcs8";
				String aliasCertifTopic=topicName+"ALIAS_TOPIC_CERTIF";
				String aliasKeyTopic=topicName+"ALIAS_TOPIC_PRIVATE_KEY";
				
				addX509ToKeyStore(pass,pathCertifTopic,dirStore,aliasCertifTopic);
				addPrivateKet(pass, aliasCertifTopic, aliasKeyTopic, pathkeyTopic,dirStore);

			}
			
			
					} catch (Exception e) {
			e.printStackTrace();
		}  
	}
	
	 
	
	
	public   void createKeyStore(String passe, String path) {
		KeyStore ks;
		try {
			ks = KeyStore.getInstance(KeyStore.getDefaultType());
			char[] password = passe.toCharArray();
			ks.load(null, password);
			FileOutputStream fos = new FileOutputStream(path);
			ks.store(fos, password);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private   InputStream fullStream(String fname) throws IOException {
		FileInputStream fis = new FileInputStream(fname);
		DataInputStream dis = new DataInputStream(fis);
		byte[] bytes = new byte[dis.available()];
		dis.readFully(bytes);
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		return bais;
	}
	
	public   boolean  addX509ToKeyStore(String passe, String pathCer, String pathKeyStore, String aliase) {
		String certfile = pathCer;
		try {
			FileInputStream is = new FileInputStream(pathKeyStore);

			KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(is, passe.toCharArray());

			String alias = aliase;
			char[] password = passe.toCharArray();

			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			InputStream certstream = fullStream(certfile);
			Certificate certs = cf.generateCertificate(certstream);

			File keystoreFile = new File(pathKeyStore);
			// Load the keystore contents
			FileInputStream in = new FileInputStream(keystoreFile);
			keystore.load(in, password);
			in.close();

			// Add the certificate
			keystore.setCertificateEntry(alias, certs);

			// Save the new keystore contents
			FileOutputStream out = new FileOutputStream(keystoreFile);
			keystore.store(out, password);
			out.close();
			 
			
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}

	}
	
	
	public   void addPrivateKet(String pass, String aliasCertifica, String aliaceKey, String filename,String keyStoteAPth) throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException, InvalidKeySpecException,Exception {
		KeyStore ks = loadKeyStore(pass,keyStoteAPth);
		Certificate[] certArray = new Certificate[1];
		certArray[0] = getX509ToKeyStore(pass, aliasCertifica,keyStoteAPth);

		File f = new File(filename);
		FileInputStream fis = new FileInputStream(f);
		DataInputStream dis = new DataInputStream(fis);
		byte[] keyBytes = new byte[(int) f.length()];
		dis.readFully(keyBytes);
		dis.close();

		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PrivateKey key = kf.generatePrivate(spec);

		boolean isValid=verifyKey(key,(X509Certificate)certArray[0]);
		if(isValid){
			ks.setKeyEntry(aliaceKey, key, pass.toCharArray(), certArray);
			updateKeyStore(pass, ks,keyStoteAPth);
		}else {
			throw new Exception("Rong ket");
		}
		
	}
	
	 
	

	public   KeyStore loadKeyStore(String pass,String keyStoteAPth) throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		 
		FileInputStream is = new FileInputStream(keyStoteAPth);
		KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
		keystore.load(is, pass.toCharArray());
		is.close();
		return keystore;
	}
	
	private   boolean verifyKey(PrivateKey key,X509Certificate aliasCertifica) {
		String s="dalam";
		try {
			return new String(decrypt(encrypt(s.getBytes(), aliasCertifica.getPublicKey()),key)).equals(s);
		} catch (Exception e) {
			return false;
		}
	}
	
	public   byte[] encrypt(byte[] text, PublicKey key) {
		byte[] cipherText = null;
		try {
			final Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText = cipher.doFinal(text);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cipherText;
	}

	public   byte[] decrypt(byte[] text, PrivateKey key) {
		byte[] dectyptedText = null;
		try {
			final Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, key);
			dectyptedText = cipher.doFinal(text);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return dectyptedText;
	}
	
	public   Certificate getX509ToKeyStore(String passe, String alias,String keyStoteAPth) {
		try {
			KeyStore keystore = loadKeyStore(passe,keyStoteAPth);
			Certificate certs = keystore.getCertificate(alias);
			return certs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public   void updateKeyStore(String pass, KeyStore keyStore,String keyStoteAPth) throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		char[] password = pass.toCharArray();
		FileOutputStream out = new FileOutputStream( keyStoteAPth);
		keyStore.store(out, password);
		out.close();
	}


}
