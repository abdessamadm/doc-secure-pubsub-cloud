package xacmlR;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.att.research.xacml.api.Request;
import com.att.research.xacml.api.Response;
import com.att.research.xacml.api.pdp.PDPEngine;
import com.att.research.xacml.api.pdp.PDPEngineFactory;
import com.att.research.xacml.std.dom.DOMRequest;

public class Principale {
	private static final Log logger	= LogFactory.getLog(Principale.class);
	
	public static void main(String[] args) {
		Path path=Paths.get("/Users/absessamadmektoubi/Documents/projet/yassine/gestion des  apple d'offre/workspace/XACML/ws/xacmlR/src/main/resources/request/IIA001Request.xml");
		try {
			new Principale().sendRequest(path);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void sendRequest(Path file) throws Exception {
		logger.info(file.toString());
		// Generate the request
		Request request  = DOMRequest.load(file.toFile());
		System.out.println("ReadRequest ok");
		PDPEngineFactory factory = PDPEngineFactory.newInstance();
		
		PDPEngine engine = factory.newEngine();
		
		Response response  = engine.decide(request);
		System.out.println(response.getResults());
		
		//this.processResponse(file, request, response, group, requestCount);
		
		
	}

}
